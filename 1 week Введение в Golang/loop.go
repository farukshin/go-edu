package main

import "fmt"

func main() {
	// цикл без условия, while(true) OR for(;;;)
	for {
		fmt.Println("loop iteration")
		break
	}

	// цикл без условия, while(isRun)
	isRun := true
	for isRun {
		fmt.Println("loo iteration with condition")
		isRun = false
	}

	for i := 0; i < 2; i++ {
		fmt.Println("Loop iteration ", i)
		if i == 1 {
			continue
		}
	}

	sl := []int{1, 2, 3}
	ind := 0
	for ind < len(sl) {
		fmt.Println("while-stype loop, ind:", ind, "value:", sl[ind])
		ind++
	}

	for i := 0; i < len(sl); i++ {
		fmt.Println("c-style loop", i, sl[i])
	}

	for ind := range sl {
		fmt.Println("range slice by index", ind)
	}
	for ind, val := range sl {
		fmt.Println("range slice by index-value", ind, val)
	}

	// операции по map
	profile := map[int]string{1: "Vasily", 2: "Romanov"}

	for key := range profile {
		fmt.Println("range map by key", key)
	}

	for key, val := range profile {
		fmt.Println("range map by key-val", key, val)
	}

	for _, val := range profile {
		fmt.Println("range map by val", val)
	}

	str := "Привет мир!"
	for pos, char := range str {
		fmt.Println("%#U at pos %d\n", char, pos)
	}

}
