package main

import "fmt"

func main() {
	// инициализация при создании
	var user map[string]string = map[string]string{
		"name":     "Vasily",
		"lastName": "Romanov",
	}
	// сразу с нужной ёмкостью
	profile := make(map[string]string, 10)

	mapLength := len(user)
	fmt.Println("%d %+v\n", mapLength, profile)

	// если ключа нет, вернет значение по умолчанию для типа
	mName := user["middleName"]
	fmt.Println("mName: ", mName)

	// проверка на существование ключа
	mName, mNameExist := user["middleName"]
	fmt.Println("mNameExist: ", mNameExist)

	// пустая переменная - только проверяем что ключ есть
	_, mNameExist2 := user["middleName"]
	fmt.Println("mNameExist2: ", mNameExist2)

	// удаление ключа
	delete(user, "lastName")
	fmt.Println("%#v\n", user)
}
