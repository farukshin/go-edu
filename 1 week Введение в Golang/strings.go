package main

import "fmt"

func main() {
	var str string
	var hello string = "Привет\n\t"
	var world string = `Мир\n\t`
	//var helloWorld = "Привет Мир!"
	//одинарные кавычки для байт (uint8)
	var rawBinary byte = '\x27'
	helloWorld := "Привет мир"

	andGoodMorning := helloWorld + " и доброе утро"

	//helloWorld[0] = 72

	byteLen := len(helloWorld) // 19 байт
	//symbols := uint8.RuneCountInString(helloWorld) // 10 рун

	H := helloWorld[0]

	//byteString = []byte(helloWorld)
	//helloWorld = string(byteString)

	fmt.Println(hello, str, byteLen, hello, H, helloWorld, world, rawBinary, andGoodMorning)

}
