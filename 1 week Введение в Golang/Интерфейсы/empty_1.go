package main

import (
	"fmt"
	"strconv"
)

type Wallet struct {
	Cash int
}

func (w *Wallet) String() string {
	return "Кошелек, в котором " + strconv.Itoa(w.Cash) + " денег"
}

func main() {
	myWallet := &Wallet{Cash: 100}
	fmt.Printf("Raw payment: %#v\n", myWallet)
	fmt.Printf("Способ оплаты: %s\n", myWallet)
}
