package main

import "fmt"

type Phone struct {
	Money   int
	AppleID string
}

type Payer interface {
	Pay(int) error
}

type Ringer interface {
	Ring(string) error
}

type NFCPhone interface {
	Payer
	Ringer
}

func (p *Phone) Pay(ammount int) error {
	if p.Money < ammount {
		return fmt.Errorf("Не хватает денег в кошельке")
	}
	p.Money -= ammount
	return nil
}

func (p *Phone) Ring(number string) error {
	if number == "" {
		return fmt.Errorf("Некорректный номер")
	}
	return nil
}

func PayForMetwiWithPhone(phone NFCPhone) {
	err := phone.Pay(1)
	if err != nil {
		fmt.Printf("Ошибка при оплате %v\n\n", err)
	}
	fmt.Printf("Турникет открыт через %T", phone)
}

func main() {
	myPhone := &Phone{Money: 9}
	PayForMetwiWithPhone(myPhone)
}
