package main

import "fmt"

type Wallet struct {
	Cash int
}

func (w *Wallet) Pay(ammount int) error {
	if w.Cash < ammount {
		return fmt.Errorf("Не хватает денег на кошельке")
	}
	w.Cash -= ammount
	return nil
}

type Card struct {
	Balance    int
	ValidUntil string
	Cardholder string
	CVV        string
	Number     string
}

func (c *Card) Pay(ammount int) error {
	if c.Balance < ammount {
		return fmt.Errorf("Не хватает денег на карте")
	}
	c.Balance -= ammount
	return nil
}

type ApplePay struct {
	Money   int
	AppleID string
}

func (a *ApplePay) Pay(ammount int) error {
	if a.Money < ammount {
		return fmt.Errorf("Не хвататет денег на аккаунте")
	}
	a.Money -= ammount
	return nil
}

type Payer interface {
	Pay(int) error
}

func Buy(p Payer) {
	err := p.Pay(10)
	if err != nil {
		fmt.Printf("Ошибка при оплате %T: %v\n\n", p, err)
		return
	}
	fmt.Printf("Спасибо за покупку через %T\n\n", p)
}

func main() {
	myWallet := &Wallet{Cash: 100}
	Buy(myWallet)

	var myMoney Payer
	myMoney = &Card{Balance: 100, Cardholder: "rvasily"}
	Buy(myMoney)

	myMoney = &ApplePay{Money: 9}
	Buy(myMoney)
}
