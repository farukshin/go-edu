package main

import "fmt"

func main() {
	buf := []int{1, 2, 3, 4, 5} // [1, 2, 3, 4, 5]
	fmt.Println(buf)

	sl1 := buf[1:4] // [2, 3, 4]
	sl2 := buf[:2]  // [1, 2]
	sl3 := buf[2:]  // [3, 4, 5]
	fmt.Println(sl1, sl2, sl3)

	newBuf := buf[:] // [1, 2, 3, 4, 5]
	newBuf[0] = 9

	newBuf = append(newBuf, 6)
	//buf = [9, 2, 3, 4, 5]
	//newBuf = [1, 2, 3, 4, 5, 6]

	newBuf[0] = 1
	fmt.Println("buf", buf)
	fmt.Println("newBuf", newBuf)

	// неправильно
	var emtyBuf []int // len=0, cap=0
	copied := copy(emtyBuf, buf)
	fmt.Println(copied, emtyBuf)

	// правильно
	newBuf = make([]int, len(buf), len(buf))
	copy(newBuf, buf)
	fmt.Println(newBuf)

	ints := []int{1, 2, 3, 4}
	copy(ints[1:3], []int{5, 6}) // ints = [1, 5, 6, 4]
	fmt.Println(ints)
}
