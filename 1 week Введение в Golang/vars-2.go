package main

import "fmt"

func main() {
	// int - платформозависимый тип, 32/64
	var i int = 10
	// автоматически выбранный int
	var autoInt = -10
	var bigInt int64 = 1<<32 - 1
	//платформозависимый int
	var unsignedInt uint = 100500
	var unsignedBigInt uint64 = 1<<64 - 1
	fmt.Println(i, autoInt, bigInt, unsignedInt, unsignedBigInt)

	// float32, float64
	var pi float32 = 3.141
	var e = 2.718
	goldenRatio := 1.618

	fmt.Println(pi, e, goldenRatio)

	// bool
	var b bool
	var isOk bool = true
	var sucess = true
	cond := true
	fmt.Println(b, isOk, sucess, cond)

	//complex64 complex128
	var c complex128 = -1.1 + 7.12i
	c2 := -1.1 + 7.12i
	fmt.Println(c, c2)
}
