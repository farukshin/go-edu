package main

func main() {
	a := 2
	b := &a
	*b = 3 // a=3
	c := &a

	d := new(int)
	*d = 12
	*c = *d // c=12 a=12
	*d = 13 // c и a  не меняются

	c = d   // теперь указатель c  указывает туда же куда и указатель defer
	*c = 14 // c=14 d=14 a=12

}
