package main

import "fmt"

func main() {
	var num0 int

	var num1 int = 1

	var num2 = 20
	fmt.Println(num0, num1, num2)

	num := 30
	num += 1
	fmt.Println("+=", num)

	var weigth, heigth int = 10, 20
	weigth, heigth = 11, 21

	// короткое присваивание
	// хотябы одна переменная должны быть новой
	weigth, age := 12, 22
	fmt.Println(weigth, heigth, age)
}
